German // English // Spanish


USB DRIVERS 3.0: HINWEISE ZU DOWNLOAD UND INSTALLATION
======================================================

Laden Sie die Datei "USB DRIVERS 3.0.ZIP" auf Ihren PC herunter.


Nur f�r Windows 8, Windows 7 und Windows Vista:
-----------------------------------------------

1) �ffnen Sie die Datei "USB DRIVERS 3.0.ZIP" per Doppelklick.

2) �ffene Sie den Ordner "USB Drivers 3.0" per Doppelklick und 
f�hren Sie die Datei SETUP.EXE aus.

Das Setup leitet Sie durch mehrmaliges Klicken auf "Weiter" durch 
den Installationsvorgang. 

Einen Hinweis auf einen nicht signierten Treiber best�tigen Sie 
bitte mit "Diese Treibersoftware trotzdem installieren".

Nach Klick auf "Fertigstellen" ist die Installation abgeschlossen.

3) Verbinden Sie die Hardware mit einem freien USB Port auf Ihrem PC.


Nur f�r Windows XP:
-------------------

1) �ffnen Sie die Datei "USB DRIVERS 3.0.ZIP" per Doppelklick.

2) Kopieren Sie den Ordner "USB Drivers 3.0" an einen Ort auf Ihrem PC

3) Verbinden Sie nun die Hardware mit einem freien USB Port auf Ihrem PC. 

Im Dialog zur Installation des Treibers w�hlen Sie die Option:
"Nein, diesmal nicht"

Im n�chsten Dialog w�hlen Sie bitte die Option:
"Software von einer Liste oder bestimmten Quelle installieren"

Klicken Sie im darauf folgenden Fenster auf 
"Folgende Quelle ebenfalls durchsuchen" und w�hlen Sie den Pfad, wohin
Sie den Ordner "USB Drivers 3.0" in Punkt 2) kopiert haben.

Nach Fertigstellung der Installation erscheint abermals ein Dialogfenster.
Wiederholen Sie die einzelnen Schritte nochmals. 

Nach dem zweiten Durchlauf ist die Installation abgeschlossen.

............................................................................


USB DRIVERS 3.0: DOWNLOAD AND INSTALLATION INSTRUCTIONS
=======================================================

Download the file "USB DRIVERS 3.0.ZIP" to your PC.


Only for Windows 8, Windows 7 and Windows Vista:
-----------------------------------------

1) Open the file "USB DRIVERS 3.0.ZIP" by double-clicking it.

2) Open the folder "USB Drivers 3.0" by double-clicking it, 
and run the file SETUP.EXE

Click "Continue" several times as Setup takes you through the 
installation process. 

When the notification regarding the unsigned driver is displayed, ,
please confirm it by selecting "Install this driver software anyway".

The installation is completed by clicking "Finish".

3) Connect the hardware to an available USB port on your PC.



Only for Windows XP:
------------------------

1) Open the file "USB DRIVERS 3.0.ZIP" by double-clicking it.

2) Copy the folder "USB Drivers 3.0" to a location on your PC.

3) Connect the hardware to an available USB port on your PC. 

In the driver installation dialogue, select the option:
"No, not this time"

In the next dialogue, please select the option:
"Install from a list or specific location (Advanced)"

In the dialogue that follows, check the box next to 
"Include this location in the search" and select the path where the 
folder "USB Drivers 3.0" was copied to in step 2).

Another dialogue is displayed after the installation is complete.
Repeat the series of steps again. 

The installation is complete after the process is completed 
the second time.

.............................................................................


USB DRIVERS 3.0: INSTRUCCIONES PARA LA DESCARGA Y LA INSTALACI�N
================================================================

Descargue el archivo "USB DRIVERS 3.0.ZIP" en su PC.


S�lo para Windows 8, Windows 7 y Windows Vista:
----------------------------------------

1) Abra el archivo "USB DRIVERS 3.0.ZIP" haciendo doble clic sobre el mismo.

2) Abra el archivo "USB DRIVERS 3.0." haciendo doble clic sobre el mismo 
y ejecute el archivo SETUP.EXE.

Durante el setup haga clic en "siguiente" tantas veces como sea necesario 
hasta completar el proceso de instalaci�n. 

Si se le muestra una advertencia sobre el hecho de que Windows no puede 
comprobar el editor de ese software, haga clic sobre "Instalar este software 
de controlador de todas formas".

Haga clic sobre "Finalizar" para concluir con el proceso de instalaci�n.

3) Conecte el hardware a su PC utilizando para ello un puerto USB.


S�lo para Windows XP:
-----------------------

1) Abra el archivo "USB DRIVERS 3.0.ZIP" haciendo doble clic sobre el mismo.

2) Copie el archivo USB Drivers 3.0 en su PC.

3) Conecte ahora el hardware a su PC utilizando para ello un puerto USB. 

Seleccione en el cuadro de di�logo de la instalaci�n del controlador 
la opci�n: "No, esta vez no"

Seleccione en el cuadro de di�logo siguiente: "Instalar software de una lista 
o una ubicaci�n determinada"

Haga clic en la siguiente ventana en "Explorar tambi�n la siguiente ubicaci�n" 
y seleccione la ruta donde copi� la carpeta "USB DRIVERS 3.0" (punto 2).

Una vez que se haya completado la instalaci�n se mostrar� un cuadro de di�logo.
Repita otra vez los pasos uno a uno. 

Cuando haya repetido todos los pasos, finalizar� la instalaci�n.

...............................................................................



